import React from "react";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

import styles from "./tweet-form.module.scss";
import { addTweet } from "../../actions";

class TweetForm extends React.Component {
  handleSave = () => {
    const { title, description, imageUrl } = this.state;
    if (!title || !description || !imageUrl) {
      console.log("Need Data!");
      return;
    }
    this.saveTweet(title, description, imageUrl);
    console.log("Form Saved!");
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  saveTweet = (title, description, imageUrl) => {
    const tweet = {
      title,
      description,
      imageUrl
    };
    addTweet(tweet).then(() => this.setState(this.props.onClose));
  };

  state = {
    title: "",
    description: "",
    imageUrl: ""
  };
  render() {
    const { isOpen, onClose } = this.props;

    return (
      <Modal open={isOpen} onClose={onClose}>
        <div className={styles.modal}>
          <div className={styles.form}>
            <TextField
              className={styles.input}
              label="Title"
              variant="outlined"
              required
              value={this.state.title}
              onChange={this.handleChange("title")}
            />
            <TextField
              className={styles.input}
              label="Description"
              multiline
              variant="outlined"
              required
              value={this.state.description}
              onChange={this.handleChange("description")}
            />
            <TextField
              className={styles.input}
              label="Image url"
              variant="outlined"
              required
              value={this.state.imageUrl}
              onChange={this.handleChange("imageUrl")}
            />
          </div>
          <div className={styles.actions}>
            <Button
              className={styles.button}
              variant="outlined"
              onClick={onClose}
            >
              Cancel
            </Button>
            <Button
              className={styles.button}
              variant="outlined"
              color="primary"
              onClick={this.handleSave}
            >
              Save
            </Button>
          </div>
        </div>
      </Modal>
    );
  }
}

export default TweetForm;

import * as firebase from "firebase/app";
import "firebase/database";

// Initialize Firebase
var FirebaseConfig = {
  apiKey: "AIzaSyBbZ108hW7yhAwyDQqV4b3TwHpDT5fZO0I",
  authDomain: "traveled-places.firebaseapp.com",
  databaseURL: "https://traveled-places.firebaseio.com",
  projectId: "traveled-places",
  storageBucket: "traveled-places.appspot.com",
  messagingSenderId: "433160144878"
};

firebase.initializeApp(FirebaseConfig);

const databaseRef = firebase.database().ref();

export const tweetsRef = databaseRef.child("tweets");
export const getTweetRef = tweetId => databaseRef.child(`tweets/${tweetId}`);
